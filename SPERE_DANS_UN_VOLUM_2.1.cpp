/* 
 * File:   SPHERE_DANS_UN_VOLUM.cpp
 * Author: Jason PUEL
 *
 * Created on 7 novembre 2017, 14:26
 */
//LIB : SOIL, glfw3, glu32, opengl32, gdi32
#include <cstdlib>
#include <iostream>
#include <math.h>
#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "GLFW/glfw3.h"
#include "SOIL.h"
#include <conio.h>
using namespace std;
#define Demi_Taille 15
//=======STRUCTURE========================
struct T_Vecteur
{float x=0,y=0,z=0;};
//----------------------------------------
struct T_Sphere
{float rayon=0.5;
float masse;
T_Vecteur Position;
T_Vecteur Vitesse;
float Couleur[4]={1,1,1,0};};
//----------------------------------------
struct T_MAILLON
{T_Sphere Sphere;
T_MAILLON* Suivant;};
//=======OPERATEURS=======================
void operator +=(T_Vecteur& P_Vecteur_1, const T_Vecteur& P_Vecteur_2)
{P_Vecteur_1.x += P_Vecteur_2.x;
P_Vecteur_1.y += P_Vecteur_2.y;
P_Vecteur_1.z += P_Vecteur_2.z;}
//----------------------------------------
T_Vecteur operator +(const T_Vecteur& P_Vecteur_1, const T_Vecteur& P_Vecteur_2)
{T_Vecteur L_Vecteur;
L_Vecteur.x=P_Vecteur_1.x + P_Vecteur_2.x;
L_Vecteur.y=P_Vecteur_1.y + P_Vecteur_2.y;
L_Vecteur.z=P_Vecteur_1.z + P_Vecteur_2.z;
return L_Vecteur;}
//----------------------------------------
T_Vecteur operator*(const T_Vecteur& P_Vecteur_1,const float L_Const)
{T_Vecteur L_Vecteur;
L_Vecteur.x=P_Vecteur_1.x*L_Const;
L_Vecteur.y=P_Vecteur_1.y*L_Const;
L_Vecteur.z=P_Vecteur_1.z*L_Const;
return L_Vecteur;}
//----------------------------------------
T_Vecteur operator -(const T_Vecteur& P_Vecteur_1, const T_Vecteur& P_Vecteur_2)
{T_Vecteur L_Vecteur;
L_Vecteur.x = P_Vecteur_1.x - P_Vecteur_2.x;
L_Vecteur.y = P_Vecteur_1.y - P_Vecteur_2.y;
L_Vecteur.z = P_Vecteur_1.z - P_Vecteur_2.z;   
return L_Vecteur;}
//----------------------------------------
T_Vecteur operator -(const T_Vecteur& P_Vecteur)
{T_Vecteur L_Vecteur;
L_Vecteur.x = - P_Vecteur.x;
L_Vecteur.y = - P_Vecteur.y;
L_Vecteur.z = - P_Vecteur.z;   
return L_Vecteur;}
//----------------------------------------
float operator !(const T_Vecteur& P_Vecteur)
{return sqrtf(P_Vecteur.x *P_Vecteur.x  +P_Vecteur.y*P_Vecteur.y + P_Vecteur.z*P_Vecteur.z );}
//========================================
bool Plein_Ecran=false;
GLFWwindow* La_Fenetre=NULL;
GLUquadric* Objet_Quadric;
T_MAILLON* Debut_Liste=NULL;
//=======FONCTIONS========================
void Initialise_Sphere(T_Sphere& P_Sphere)
{P_Sphere.rayon=((rand()%10)/10.0f)+1.5; // pour éviter de futur division par Zéro
P_Sphere.masse=(4/3)*3.14*(P_Sphere.rayon*P_Sphere.rayon*P_Sphere.rayon)*0.1;
P_Sphere.Position.x=(rand()%(Demi_Taille*2))-Demi_Taille;
P_Sphere.Position.y=(rand()%(Demi_Taille*2))-Demi_Taille;
P_Sphere.Position.z=(rand()%(Demi_Taille*2))-Demi_Taille;
//P_Sphere.Position.z=0;
P_Sphere.Vitesse.x=((rand()%200)-100)/5000.0f;
P_Sphere.Vitesse.y=((rand()%200)-100)/5000.0f;
P_Sphere.Vitesse.z=((rand()%200)-100)/5000.0f;
//P_Sphere.Vitesse.z=0;
P_Sphere.Couleur[0]=(rand()%100)/100.0f;
P_Sphere.Couleur[1]=(rand()%100)/100.0f;
P_Sphere.Couleur[2]=(rand()%100)/100.0f;
}
//----------------------------------------
void ADD_Sphere(T_Sphere& P_Sphere)
{cout<<"-----GENERATION-NOUVELLE-SPHERE-----"<<endl;
cout<<"Veuillez donner une valeur de Rayon :";cin>>P_Sphere.rayon;
    while (P_Sphere.rayon<=0){//Repeter
    cout<<"Veuillez donner une valeur de Rayon supperieur a Zéro:";
    cin>>P_Sphere.rayon;};
P_Sphere.masse=(4/3)*3.14*(P_Sphere.rayon*P_Sphere.rayon*P_Sphere.rayon)*0.1;
cout<<"Position (X,Y,Z) :"<<endl;
cin>>P_Sphere.Position.x>>P_Sphere.Position.y>>P_Sphere.Position.z;
cout<<"Vitesse (X,Y,Z) :"<<endl;
cin>>P_Sphere.Vitesse.x>>P_Sphere.Vitesse.y>>P_Sphere.Vitesse.z;
P_Sphere.Vitesse=P_Sphere.Vitesse*0.001;
cout<<"Couleur (R,G,B) :"<<endl;
cin>>P_Sphere.Couleur[0]>>P_Sphere.Couleur[1]>>P_Sphere.Couleur[2];
cout<<endl;}
//----------------------------------------
void Deplacement_Sphere()
{T_MAILLON* L_Maillon=Debut_Liste;
while(L_Maillon){
    L_Maillon->Sphere.Position += L_Maillon->Sphere.Vitesse;
    L_Maillon=L_Maillon->Suivant;}}
//----------------------------------------
void Reagir_avec_le_Bord()
{T_MAILLON* L_Maillon=Debut_Liste;
while(L_Maillon){
        //--CHOQUE-EN-X---
if((L_Maillon->Sphere.Position.x+L_Maillon->Sphere.rayon)>Demi_Taille){
    L_Maillon->Sphere.Position.x=Demi_Taille-L_Maillon->Sphere.rayon;
    L_Maillon->Sphere.Vitesse.x= -L_Maillon->Sphere.Vitesse.x;};
if((L_Maillon->Sphere.Position.x-L_Maillon->Sphere.rayon)< -Demi_Taille){
    L_Maillon->Sphere.Position.x=-Demi_Taille+L_Maillon->Sphere.rayon;
    L_Maillon->Sphere.Vitesse.x= -L_Maillon->Sphere.Vitesse.x;
  };
//--CHOQUE-EN-Y---
if((L_Maillon->Sphere.Position.y+L_Maillon->Sphere.rayon)>Demi_Taille){
    L_Maillon->Sphere.Position.y=Demi_Taille-L_Maillon->Sphere.rayon;
    L_Maillon->Sphere.Vitesse.y= -L_Maillon->Sphere.Vitesse.y;};
if((L_Maillon->Sphere.Position.y-L_Maillon->Sphere.rayon)< -Demi_Taille){
    L_Maillon->Sphere.Position.y=-Demi_Taille+L_Maillon->Sphere.rayon;
    L_Maillon->Sphere.Vitesse.y= -L_Maillon->Sphere.Vitesse.y;
  };
//--CHOQUE-EN-Z---
if((L_Maillon->Sphere.Position.z+L_Maillon->Sphere.rayon)>Demi_Taille){
    L_Maillon->Sphere.Position.z=Demi_Taille-L_Maillon->Sphere.rayon;
    L_Maillon->Sphere.Vitesse.z= -L_Maillon->Sphere.Vitesse.z;};
if((L_Maillon->Sphere.Position.z-L_Maillon->Sphere.rayon)< -Demi_Taille){
    L_Maillon->Sphere.Position.z=-Demi_Taille+L_Maillon->Sphere.rayon;
    L_Maillon->Sphere.Vitesse.z= -L_Maillon->Sphere.Vitesse.z;
  };
    L_Maillon=L_Maillon->Suivant;} 
}
//----------------------------------------
void Gestion_Collision()
{if(Debut_Liste && Debut_Liste->Suivant){
    T_MAILLON* L_Maillon_1=Debut_Liste;
    while(L_Maillon_1){
        T_MAILLON* L_Maillon_2=L_Maillon_1->Suivant;
        while(L_Maillon_2){
            T_Vecteur L_Distance =  L_Maillon_2->Sphere.Position - L_Maillon_1->Sphere.Position;
            if ((!L_Distance) <= (L_Maillon_2->Sphere.rayon + L_Maillon_1->Sphere.rayon)){        //if V même sence que v : V=-V else V=V
                    //Quantiter de mouvement; Soient P=M*V, p=m*v Or PFD: P=p donc V=v(m/M) et v=V(M/m)
                    T_Vecteur L_Vitesse_Temp=L_Maillon_1->Sphere.Vitesse;
                    L_Maillon_1->Sphere.Vitesse = L_Maillon_2->Sphere.Vitesse*(L_Maillon_2->Sphere.masse / L_Maillon_1->Sphere.masse);
                    L_Maillon_2->Sphere.Vitesse = L_Vitesse_Temp*(L_Maillon_1->Sphere.masse / L_Maillon_2->Sphere.masse);};
                    //L_Maillon_1->Sphere.Vitesse = L_Maillon_1->Sphere.Vitesse*(-L_Maillon_2->Sphere.masse / L_Maillon_1->Sphere.masse);
                    //L_Maillon_2->Sphere.Vitesse = L_Maillon_2->Sphere.Vitesse*(-L_Maillon_1->Sphere.masse / L_Maillon_2->Sphere.masse);};
        L_Maillon_2=L_Maillon_2->Suivant;}
    L_Maillon_1=L_Maillon_1->Suivant;}
}}
//----------------------------------------
void Epilepsie()
{T_MAILLON* L_Maillon=Debut_Liste;
while(L_Maillon){
    L_Maillon->Sphere.Couleur[0]=(rand()%100)/100.0f;;
    L_Maillon->Sphere.Couleur[1]=(rand()%100)/100.0f;
    L_Maillon->Sphere.Couleur[2]=(rand()%100)/100.0f;
L_Maillon=L_Maillon->Suivant;}
glClearColor((rand()%100)/100.0f, (rand()%100)/100.0f, (rand()%100)/100.0f, 0.0);}
//----------------------------------------
void Text_Help()
{cout<<"******************************************************"<<endl;
cout<<"Fonction Utilisable par ce Programme :"<<endl;
cout<<"    ESCAPE = Close window"<<endl;
cout<<"    R = Add Sphere RANDOM"<<endl;
cout<<"    G = Add Sphere"<<endl;
cout<<"    J = Suppr Sphere"<<endl;
cout<<"    H = Reset Spheres"<<endl;
cout<<"    A = Epilepsie (rest enfoncer pour plus de FUN)"<<endl;
cout<<"******************************************************"<<endl;
cout<<endl;}
//===FONCTIONS=DYNAMIQUE==================
void Ajoute_Valeur_RANDOM()//ON EMPILE 
{T_MAILLON* L_Maillon = new T_MAILLON;
Initialise_Sphere(L_Maillon->Sphere);
L_Maillon->Suivant = Debut_Liste;
Debut_Liste=L_Maillon;}
//----------------------------------------
void Ajoute_Valeur_DEFINIE()//ON EMPILE 
{T_MAILLON* L_Maillon = new T_MAILLON;
ADD_Sphere(L_Maillon->Sphere);
L_Maillon->Suivant = Debut_Liste;
Debut_Liste=L_Maillon;
Text_Help();}
//----------------------------------------
void Effacer_Valeur()//ON DEPILE
{T_MAILLON* L_Maillon;
    if(Debut_Liste){
        L_Maillon=Debut_Liste->Suivant;
        delete Debut_Liste;
        Debut_Liste = L_Maillon;}}
//----------------------------------------
void Effacer_Liste()//RESET 
{T_MAILLON* L_Maillon;
    while(Debut_Liste){
        L_Maillon=Debut_Liste->Suivant;
        delete Debut_Liste;
        Debut_Liste = L_Maillon;}
glClearColor(0.9, 0.9, 0.9, 0.0);}
//----------------------------------------
void Afficher_Sphere(const T_Sphere& P_Sphere)
{ glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glMaterialfv(GL_FRONT,GL_DIFFUSE,P_Sphere.Couleur);
    glTranslatef(P_Sphere.Position.x,P_Sphere.Position.y,P_Sphere.Position.z);
    gluSphere(Objet_Quadric, P_Sphere.rayon,20,20);
    // ( ... , rayon , précision (X,Y) , précision (X,Z))
    glPopMatrix();//supprime Log identity
}
//---------------------------------------- 
void Affiche_Liste()
{T_MAILLON* L_Maillon=Debut_Liste;
while(L_Maillon){
    Afficher_Sphere(L_Maillon->Sphere);
    L_Maillon=L_Maillon->Suivant;}}
//===FONCTIONS=FENETRE==================
void Definir_Projection(int P_Largeur, int P_Hauteur)
{  glViewport(0, 0, P_Largeur, P_Hauteur);  //Definir la zone dans laquelle la projection sera effectuée
   glMatrixMode(GL_PROJECTION); // choisir comme matrice courante la matrice de projection
   glLoadIdentity(); // initialiser la matrice à la valeur unitaire
   float L_Rapport = P_Largeur / (float) P_Hauteur;
   gluPerspective(60.0,L_Rapport,1.5,100.0); //fabrique d'une matrice de projection 60° de vision rectangulaire
                                             //multiplication de cette matrice avec la matrice courante
   //fabrique d'une matrice correspondant à la position de la camera
   //multiplication de cette matrice avec la matrice courante
   gluLookAt(0.0,0.0,30.0, // Camera est en 0,0,20 (x y z)
            0.0, 0.0, 0.0, // regarde 0,0,0 (le centre)
	    0.0,1.0,0.0);  // vecteur orientation  (vers le haut)
   glMatrixMode(GL_MODELVIEW); //choisir comme matrice courante celle la plus utilisée (matrice de transformation géometrique)  
}
//---------------------------------------
void On_Erreur(int P_Code_Erreur, const char* P_Description)
{// Fonction callback invoquée par le gestionnaire de fenetre lors de l'apparission d'une erreur
    cerr<<"["<<P_Code_Erreur<<"] "<<P_Description<<endl;}
//---------------------------------------
void On_Touche_Enfoncee(GLFWwindow* P_Fenetre, int P_Touche, int P_ScanCode,int P_Action, int P_Complement)
{// Fonction callback invoquée par le gestionnaire de fenetre lors de l'appuie sur une touche du clavier
if ((P_Touche == GLFW_KEY_ESCAPE) && (P_Action == GLFW_PRESS )) 
    glfwSetWindowShouldClose(P_Fenetre, GL_TRUE); //Close Window
if ((P_Touche == GLFW_KEY_R) && (P_Action == GLFW_PRESS )) 
    Ajoute_Valeur_RANDOM();
if ((P_Touche == GLFW_KEY_G) && (P_Action == GLFW_PRESS )) 
    Ajoute_Valeur_DEFINIE();
if ((P_Touche == GLFW_KEY_H) && (P_Action == GLFW_PRESS )) 
    Effacer_Liste();
if ((P_Touche == GLFW_KEY_J) && (P_Action == GLFW_PRESS )) 
    Effacer_Valeur();
if (P_Touche == GLFW_KEY_Q) //La touche peut restée enfoncer pour pour un effet epileptique
    Epilepsie();
}
//---------------------------------------
void On_Redimensionne_Fenetre(GLFWwindow* P_Fenetre, int P_Largeur,int P_Hauteur)
{// Fonction callback invoquée lors d'un redimentionnement de fenetre
 // si la zone d'affichage a changé, la projection doit être recalculée 
 Definir_Projection(P_Largeur,P_Hauteur);}
//---------------------------------------
bool Creation_Fenetre(bool P_Plein_Ecran)
{if (P_Plein_Ecran) {
    GLFWmonitor* L_Ecran= glfwGetPrimaryMonitor();
    const GLFWvidmode* L_Mode_Affichage = glfwGetVideoMode(L_Ecran);
    La_Fenetre= glfwCreateWindow(L_Mode_Affichage->width, 
    L_Mode_Affichage->height, "CSI_3 2017: PROJET Jason PUEL", L_Ecran, NULL);} 
else {La_Fenetre= glfwCreateWindow(1024, 768, "CSI_3 2017: PROJET Jason PUEL", NULL, NULL);}
if (La_Fenetre) {
    glfwMakeContextCurrent(La_Fenetre);
  }}
//---------------------------------------
void Init_Fenetre()
{glfwSetErrorCallback(On_Erreur);
 int L_OK= glfwInit(); 
 if (L_OK == GL_FALSE) exit(0);
 Creation_Fenetre(Plein_Ecran);
 if (La_Fenetre==NULL) {
    glfwTerminate();
    exit(0);}
 glfwSetWindowSizeCallback(La_Fenetre,On_Redimensionne_Fenetre );    
 glfwSetKeyCallback(La_Fenetre, On_Touche_Enfoncee);
 //glfwSwapInterval(1);
}
//---------------------------------------
void Fin_Fenetre()
{glfwDestroyWindow(La_Fenetre);
    glfwTerminate();}
//---------------------------------------
void Init_Rendu()
{ glClearColor(0.9, 0.9, 0.9, 0.0); // Couleur d'effacement de la zone de rendu
  glShadeModel(GL_SMOOTH);          // lorsque des points sont de couleurs différentes, 
                                    //une interpolation de couleur est réalisée pour remplir la surface les séparant
  
  //lors de la projection la profondeur est traduite par le supperposition des points.
  // La superposition est représentée par une profondeur e [0,1]
  // le buffeur de profondeur est initialisé avec la valeur 1 (1 => profond, 0=> devant)
  glClearDepth(1.0f);
  glDepthFunc(GL_LESS); // le rendu est réalisé en affichant les points par ordre décroissant :
  glEnable(GL_DEPTH_TEST);// le test de profondeur est actif (3D. peut être inactif pour la 2D)
  }
//---------------------------------------
void Init_Lumiere()
{glEnable(GL_LIGHTING);// Active la gestion des lumières
 glEnable(GL_LIGHT0);// Allume la lumière 0
}
//---------------------------------------
void Creation_Objet_3D()
{Objet_Quadric= gluNewQuadric();}
//---------------------------------------
void Destruction_Objet_3D()
{gluDeleteQuadric(Objet_Quadric);}
//---------------------------------------
void Deplace_Les_Spheres()
{Gestion_Collision();
Deplacement_Sphere();
Reagir_avec_le_Bord();
Affiche_Liste();
Sleep(0.01);}
//=====PROGRAMME=PRINCIPALE===============
int main() {
    Text_Help();
    Init_Fenetre();
    Definir_Projection(1024,768);
    Init_Rendu();
    Init_Lumiere();
    Creation_Objet_3D();
    glMatrixMode(GL_MODELVIEW);
    while (!glfwWindowShouldClose(La_Fenetre)) {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT); // effacer les buffers de rendu
        glLoadIdentity();
        glPushMatrix(); //Save possition
        
        Deplace_Les_Spheres();
        
        glfwSwapBuffers(La_Fenetre);
        glfwPollEvents();}
    Destruction_Objet_3D();
    Fin_Fenetre();
  return 0;}